import socket
import threading

def handle_client(sock_client, addr):
    ##logging.debug('client connection accepted: {}'.format(addr))
    message = sock_client.recv(1024)
    server_message = "Selamat Datang"
    sock_client.send(server_message)
    while True :
        sock_client.send("Masukkan Angka Kode Operasi ")
        sock_client.send("1.Peregi ")
        sock_client.send("2.Peersegi Panjang ")
        sock_client.send("3.Lingkaran ")
        sock_client.send("0.Keluar ")
        message = client.recv(1024)
        if(message == "1"):
            server_message = "Input Nilai Panjang "
            sock_client.send(server_message)
            message = client.recv(1024)
            panjang = raw_input(message)
            luas = panjang * panjang
            server_message = "Luas Persegi Adalah : "
            sock_client.send(server_message)
            sock_client.send(luas)
        elif(message == "2"):
            server_message = "Input Nilai Panjang "
            sock_client.send(server_message)
            message = client.recv(1024)
            panjang = raw_input(message)
            server_message = "Input Nilai Lebar "
            sock_client.send(server_message)
            message = client.recv(1024)
            lebar = raw_input(message)
            luas = panjang * lebar
            server_message = "Luas Persegi Adalah : "
            sock_client.send(server_message)
            sock_client.send(luas)
        elif(message == "3"):
            server_message = "Input Nilai Diameter "
            sock_client.send(server_message)
            message = client.recv(1024)
            diameter = raw_input(message)
            jari = diameter / 2
            luas = 3.14 * jari * jari
            server_message = "Luas Lingkaran Adalah : "
            sock_client.send(server_message)
            sock_client.send(luas)
        else :
            sock_client.send("Inputan Anda Salah")
            break   
    sock_client.close()

sock_server = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
sock_server.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
sock_server.bind(('127.0.0.1',9999))
sock_server.listen(5)
clients = []
while True:
    try:
        s, a = sock_server.accept()
        t_client = threading.Thread(target=handle_client, args=(s,a))
        t_client.start()
        t_client.join(1)
        clients.append(t_client)
    except Exception as e:
        logging.debug(e)
        break
    except KeyboardInterrupt as k:
        break
